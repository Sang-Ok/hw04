// simple_int_set.cc

#ifndef _SIMPLE_INT_SET_CC_
#define _SIMPLE_INT_SET_CC_

#include <algorithm>
#include "simple_int_set.h"
void SimpleIntSet::Set(const int* values, size_t size){
	for(int i=0; i<size; i++){
		values_.push_back(values[i]);
	}
}

SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const{
	SimpleIntSet return_set;
	int i,j;
	for(i=0; i< values_.size();  i++)
		for(j=0;j<int_set. values_.size();j++)
			if(values_[i]==int_set.values_[j]){
				return_set.values_.push_back(values_[i]);
				break;
			}
	return return_set;
}

bool which_is_big(int i, int j){return i<j;}

SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const{
	SimpleIntSet return_set;
	int i,j,temp;
	bool intersect;
	for(i=0; i< values_.size();i++)
		return_set.values_.push_back(values_[i]);
	
	for(i=0; i<int_set. values_.size();  i++){
		intersect=false;
		for(j=0; j< values_.size();  j++)
			if(int_set.values_[i]==values_[j]){
				intersect=true;
				break;
			}
		if(intersect==false)
		{
			//TEST("JUST B4 ADDING :",int_set.values_[i])
			return_set.values_.push_back(int_set.values_[i]);
		}
	}

//sort()를 활성화시키면 바로 에러발생. 마지막 파라미터를 생략해도 똑같이 컴파일불가. hw04-03과 유사한 반응임.
//	sort(values_.begin(), values_.end(),which_is_big);
	return return_set;
}

SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const{
	SimpleIntSet return_set;
	int i,j;
	bool ok;
	for(i=0; i< values_.size();i++){
		ok=true;
		for(j=0;j<int_set. values_.size();j++)
			if(values_[i]==int_set.values_[j]){				
				ok=false;
				break;
			}
		if(ok==true)
			return_set.values_.push_back(values_[i]);
	}
	return return_set;
}

#endif //_SIMPLE_INT_SET_CC_
